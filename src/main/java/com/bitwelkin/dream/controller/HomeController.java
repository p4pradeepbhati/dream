/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitwelkin.dream.controller;

import com.bitwelkin.dream.model.Record;
import com.bitwelkin.dream.service.RecordService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author pradeep
 */
@Controller
public class HomeController {

    @Autowired
    private RecordService recordService;

    @RequestMapping("/")
    public String homePage(Model model) {
        Record record = new Record();
        model.addAttribute("record", record);
        return "home";
    }

    @RequestMapping("/list")
    public String listData(Model model) {
        List<Record> records = recordService.getAllRecords();
        model.addAttribute("records", records);
        return "list";
    }
    
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String saveRecord(@ModelAttribute("record") Record record){
        recordService.saveOrUpdateCart(record);
        return "redirect:/";
    }
}

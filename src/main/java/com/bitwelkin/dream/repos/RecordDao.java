package com.bitwelkin.dream.repos;

import com.bitwelkin.dream.model.Record;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Pradeep Bhati
 */
@Repository
public interface RecordDao extends JpaRepository<Record, Integer> {

//    @Query("FROM Cart WHERE userId=:id")
//    public List<Cart> getCartItemsByUserId(@Param("id") int id);
}

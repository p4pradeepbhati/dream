/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitwelkin.dream.service;

import com.bitwelkin.dream.model.Record;
import java.util.List;

/**
 *
 * @author Pradeep Bhati
 */
public interface RecordService {

    public void saveOrUpdateCart(Record record);

    public List<Record> getAllRecords();

}

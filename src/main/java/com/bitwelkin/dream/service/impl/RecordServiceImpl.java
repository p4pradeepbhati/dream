package com.bitwelkin.dream.service.impl;

import com.bitwelkin.dream.model.Record;
import com.bitwelkin.dream.repos.RecordDao;
import com.bitwelkin.dream.service.RecordService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Pradeep Bhati
 */
@Service("recordService")
@Transactional(readOnly = true)
public class RecordServiceImpl implements RecordService {

    @Autowired
    private RecordDao recordDao;

    @Override
    @Transactional(readOnly = false)
    public void saveOrUpdateCart(Record record) {
        recordDao.save(record);
    }

    @Override
    public List<Record> getAllRecords() {
        return recordDao.findAll();
    }

}

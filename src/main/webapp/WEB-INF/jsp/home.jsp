<%-- 
    Document   : home
    Created on : 30 Apr, 2020, 3:47:19 PM
    Author     : pradeep
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

    </head>
    <body>
        <link rel="stylesheet" href="<c:url value="/assets/css/vendor/bootstrap.min.css"/>">
        <script src="<c:url value="/assets/js/main.js"/>"></script>
        <h1>Add Detail</h1>
        <div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <form:form action="" method='POST' modelAttribute="record">
                            <form:input type='hidden' path="id"/>
                            <div class="mb-3">
                                <form:label path="name" cssClass="control-label">Name</form:label>
                                    <div class="">
                                    <form:input class="form-control"  type='text' path="name"/>
                                </div>
                            </div>
                            <div class="mb-3">
                                <form:label path="extraName" cssClass="control-label">Father Name</form:label>
                                    <div class="">
                                    <form:input class="form-control" type='text' path="extraName"/>
                                </div>
                            </div>
                            <div class="mb-3">
                                <form:label path="age" cssClass="control-label">age</form:label>
                                    <div class="">
                                    <form:input class="form-control" type='number' path="age"/>
                                </div>
                            </div>
                            <div class="mb-3">
                                <form:label path="mobile" cssClass="control-label">mobile</form:label>
                                    <div class="">
                                    <form:input class="form-control" type='text' path="mobile"/>
                                </div>
                            </div>
                            <div class="mb-3">
                                <form:label path="add" cssClass="control-label">add</form:label>
                                    <div class="">
                                    <form:input class="form-control"  type='text' path="add"/>
                                </div>
                            </div>
                            <div class="mb-3">
                                <form:label path="disct" cssClass="control-label">disct</form:label>
                                    <div class="">
                                    <form:input class="form-control"  type='text' path="disct"/>
                                </div>
                            </div>
                            <div class="mb-3">
                                <form:label path="state" cssClass="control-label">state</form:label>
                                    <div class="">
                                    <form:input class="form-control"  type='text' path="state"/>
                                </div>
                            </div>
                            <div class="mb-3">
                                <form:label path="reason" cssClass="control-label">reason</form:label>
                                    <div class="">
                                    <form:input class="form-control"  type='text' path="reason"/>
                                </div>
                            </div>
                            <div class="mb-3">
                                <form:label path="disease" cssClass="control-label">disease</form:label>
                                    <div class="">
                                    <form:input class="form-control"  type='text' path="disease"/>
                                </div>
                            </div>
                            <button class="btn btn-primary btn-lg btn-block" type="submit">Save</button>
                        </form:form>
                    </div>
                </div>    
            </div>
        </div>
    </body>
</html>

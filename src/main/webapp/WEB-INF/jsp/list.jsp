<%-- 
    Document   : home
    Created on : 30 Apr, 2020, 3:47:19 PM
    Author     : pradeep
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
    </head>
    <body>
        <link rel="stylesheet" href="<c:url value="/assets/css/vendor/bootstrap.min.css"/>">
        <script src="<c:url value="/assets/js/main.js"/>"></script>
        <h1>show list here</h1>
        <c:forEach items="${records}" var="r">
            <p>${r.name}</p>
        </c:forEach>
    </body>
</html>
